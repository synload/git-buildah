FROM alpine:latest

RUN apk install buildah

ADD start.sh /start.sh

WORKDIR /code

CMD /start.sh
